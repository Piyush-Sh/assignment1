function calc() {
    var x = parseFloat(document.getElementById('input1').value);
    var y = parseFloat(document.getElementById('input2').value);
    var oper = document.getElementById('operator').value;
    if (oper == '+') {
        document.getElementById("result").innerHTML = String(x + y);
    }
    if (oper == '-') {
        document.getElementById("result").innerHTML = String(x - y);
    }
    if (oper == '/') {
        if (y == 0) {
            document.getElementById("result").innerHTML = "infinite";
        }
        else {
            document.getElementById("result").innerHTML = String(x / y);
        }
    }
    if (oper == '*') {
        document.getElementById("result").innerHTML = String(x * y);
    }
}
