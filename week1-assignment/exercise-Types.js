var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
var bankAccount;
bankAccount = {
    money: 2000,
    deposit: function (value) {
        return this.money += value;
    }
};
var myself;
myself = {
    name: "Max",
    bankAccount: bankAccount,
    hobbies: ["Sports", "Cooking"]
};
bankAccount.deposit(3000);
console.log(myself);
//2nd
function add() {
    var nums = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        nums[_i] = arguments[_i];
    }
    var sum = 0;
    var i;
    for (i = 0; i < nums.length; i++) {
        sum = sum + nums[i];
    }
    console.log("sum of all element " + nums + " are " + sum);
    return sum;
}
var ele = add(1, 2, 3, 4);